from flask import Flask, abort, render_template, request, send_from_directory
import os

app = Flask(__name__)

ill_start = ["~", "//", ".."]

@app.route("/")
def no_path():
    abort(401)

@app.route("/<path:path>")
def response(path):
    print(request.__dict__)
    f_name = str(path)
    is_forbid = False
    for i in range(len(ill_start)):
        if ill_start[i] in f_name:
            is_forbid = True
    if f_name.startswith("/"):
        is_forbid = True
    if is_forbid:
        abort(403)
    else:
        try:
            if f_name.endswith(".css"):
                return send_from_directory('static/css', f_name), 200
            else:
                return render_template(f_name), 200
        except:
            abort(404)

@app.errorhandler(401)
def unauthorized(e):
    return render_template("401.html"), 401

@app.errorhandler(403)
def forbidden(e):
    return render_template("403.html"), 403

@app.errorhandler(404)
def not_found(e):
    return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port=5000)
